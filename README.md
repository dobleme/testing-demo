Continguts
==========

[comment]: <> (10')
Introducció
-----------
### Objectius del taller
- Per què pot ser important provar el nostre software? i a l'universitat?
- Aprendre a plantejar les proves dona't un problema.
- Aprendrer a fer les proves d'un mode pràctic, la teoría pels altres.
- Poder tenir unes bases amb les cuals poder continuar investigant aquest món.

[comment]: <> (15')
Què, per què i còm?
------------------
### Què és un test de software?
Un test és una prova determinada sobre una peça de software.

### Per què fer un test?
- Ajudar a mantenir la qualitat del codi.
- Ens ajuda a dissenyar i programar les soluciones del problema.
- Una forma més formal de validar les solucions.

### Com ha de ser?
- Únic
- Aïllat
- Complet
- Especific
- Determinista

[F.I.R.S.T.](https://github.com/ghsukumar/SFDC_Best_Practices/wiki/F.I.R.S.T-Principles-of-Unit-Testing) 

[comment]: <> (75')
Taller
------
### Introducció
- Les eines: PHP i PHPUnit
- La primitiva `$this->assertTrue(false)`
- Run
- El joc de la vida
  - https://github.com/fabricejeannet/kataGameOfLife/blob/master/src/test/java/Tests.java
  - https://github.com/georgicodes/conways_game_of_life/blob/master/jasmine/spec/GameSpec.js

### Tests de caixa negre
- Explicar un problema donat: "Dona't un camí ja fet en un taurell, hem de cercar el camí per tornar a la cassella de sortida dona't aquest."
- Com s'ha d'escriure un test (nomenclatura)
- Donar un exemple amb PHPUnit del més comú

### Dar cera, pulir cera
- Mostrar una solució amb l'implementació donada
  - No hi ha boundaries
  - No hi ha captura d'excepcions
  - Més edge cases en general

[comment]: <> (10')
La realitat
-----------
### Conceptes que podem aplicar
- Unit, Integration, E2E
- TDD

### El món real que més té
- E2E (Acceptació)
- Automatització del testing → CI/CD
- Com ho tenim a habitissimo
  - Seleniums
  - PHPUnit

[comment]: <> (~)
Dubtes
------

[comment]: <> (~)
Gràcies
-------
