<?php

namespace Habitissimo\Application\Lib\Validator\Tests;

use Habitissimo\Application\Lib\Validator\CIF;
use Habitissimo\Application\Lib\Validator\ValidatorRegistry;
use Habitissimo\Utils\Contract\ValidationHelper;

class CIFValidatorTest extends \Habitissimo\Tests\TestCase
{
  public function getESData()
  {
    return [
      ''            => false,
      'C92716596'   => true,
      'V3229720B'   => true,
      'v3229720b'   => true,
      'v3229720-b'  => true,
      'v3229720B  ' => true,
      'V3229721U'   => false,
      ' B/31951767' => true,
    ];
  }

  public function dataProvider()
  {
    $cifs_es = $this->getESData();

    return [
      [CIF\CIFValidatorES::class, $cifs_es],
    ];
  }

  /**
   * @dataProvider dataProvider
   */
  public function testCIFValidator($country_cif_validator_class, $country_data)
  {
    $validator = new $country_cif_validator_class;
    foreach ($country_data as $cif => $expected_result) {
      $this->assertEquals($expected_result, $validator->isValid($cif));
    }
  }
}
