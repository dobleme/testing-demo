<?php 

class Set {
    
    private $items = array();

    public function add($item) {
        if (!$this->contains($item)) $this->items[]=$item;
    }

    public function remove($item) {
        foreach ($this->items as $idx => $it) {
            if ($it->equals($item)) unset($this->items[$idx]);
        }
    }

    public function contains($item) {
        foreach ($this->items as $it) {
            if ($it->equals($item)) return true;
        }
        return false;
    }

    public function isEmpty() {
        return empty($this->items);
    }

    public function length() {
        return count($this->items);
    }
}

?>