<?php 

class MyList {

    private $list = array();

    public function add($item) {
        $this->list[] = $item;
    }

    public function get($idx) {
        return $this->list[$idx];
    }

    public function remove($item) {
        foreach ($this->list as $key => $value) {
            if ($value->equals($item)) {
                unset($this->list[$key]);
                $this->list = array_values($this->list);
            }
        }
    }

    public function removeAt($idx) {
        foreach ($this->list as $key => $value) {
            if ($key == $idx) {
                unset($this->list[$key]);
                $this->list = array_values($this->list);
            }
        }
    }

    public function indexOf($item) {
        foreach ($this->list as $key => $value) {
            if ($value->equals($item)) {
                return $key;
            }
        }
        return NULL;
    }

    public function contains($item) {
        foreach ($this->list as $value) {
            if ($value->equals($item)) return true;
        }
        return false;
    }

    public function isEmpty() {
        return empty($this->list);
    }

    public function length() {
        return count($this->list);
    }
}

?>