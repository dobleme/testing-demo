<?php 

class Stack {

    private $stack = array();
    
    public function push($item) {
        array_unshift($this->stack, $item);
    }

    public function pop() {
        if ($this->isEmpty()) {
            throw new RunTimeException('Stack is empty!');
        }
        return array_shift($this->stack);
    }

    public function top() {
        return current($this->stack);
    }

    public function isEmpty() {
        return empty($this->stack);
    }

    public function length() {
        return count($this->stack);
    }
}

?>