<?php 

class CaveNodeInteractor {
	private $visitedCells;
	private $goalPosition;

	function __construct($goalPosition, $visitedCells) {
		$this->visitedCells = $visitedCells;
		$this->goalPosition = $goalPosition;
	}

	public function getNeighbours($node) {
		$neighbours = [];
		foreach ($node->getPosition()->getNearPositions() as $neighbourPosition) {
			if ($this->visitedCells->contains($neighbourPosition)) {
				$neighbour = new CaveNode($neighbourPosition);
				$neighbours[] = $neighbour;
			}
		}
		return $neighbours;
	}

	public function getHeuristic($node) {
		$rowDistance = abs($this->goalPosition->row - $node->getPosition()->row);
		$columnDistance = abs($this->goalPosition->column - $node->getPosition()->column);
		return $rowDistance + $columnDistance;
	}

	public function isSolution($node) {
		return $node->getPosition()->equals($this->goalPosition);
	}
}
