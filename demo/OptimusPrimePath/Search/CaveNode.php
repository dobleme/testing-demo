<?php 

class CaveNode {
  private $gScore;
  private $fScore;
  private $previousNode;
  private $position;

  function __construct($position)
  {
    $this->position = $position;
    $this->gScore = PHP_INT_MAX;
    $this->fScore = PHP_INT_MAX;
  }

  public function getGScore() {
    return $this->gScore;
  }

  public function setGScore($score) {
      $this->gScore = $score;
  }

  public function getFScore() {
    return $this->fScore;
  }

  public function setFScore($score) {
    $this->fScore = $score;
  }

  public function getPreviousNode() {
    return $this->previousNode;
  }

  public function setPreviousNode($node) {
    $this->previousNode = $node;
  }

  public function getPosition() {
    return $this->position;
  }

  public function equals($node) {
    return $this->position->equals($node->position);
  }

  public function hashCode() {
    return $this->position->hashCode();
  }
}
