<?php

require_once __DIR__ . '/../Utils/Set.php';
require_once __DIR__ . '/../Utils/MyList.php';
require_once __DIR__ . '/../Utils/Stack.php';

class Search {
  private $travelCost;
  private $closed;
  private $open;
  private $nodeInteractor;

  function __construct($travelCost, $nodeInteractor) {
    $this->travelCost = $travelCost;
    $this->nodeInteractor = $nodeInteractor;
  }

  public function aStar($startNode) {
    $this->closed = new Set();
    $this->open = new MyList();

    $startNode->setGScore(0);
    $startNode->setFScore($this->nodeInteractor->getHeuristic($startNode));
    $this->open->add($startNode);

    while (!$this->open->isEmpty()) {
      $current = $this->extractLeast();
      if ($this->nodeInteractor->isSolution($current)) {
        return $this->reconstructPath($current);
      }
      $this->closed->add($current);

      $successors = $this->nodeInteractor->getNeighbours($current);
      foreach ($successors as $successor) {
        if ($this->closed->contains($successor)) {
          continue;
        }

        $tentativeGScore = $current->getGScore() + $this->travelCost;
        if ($this->open->contains($successor)) {
          $old = $this->open->get($this->open->indexOf($successor)); // Update existent node
          if ($tentativeGScore >= $old->getGScore()) {
            continue;
          }
          $successor = $old;
        } else {
          $this->open->add($successor);
        }

        $successor->setPreviousNode($current);
        $successor->setGScore($tentativeGScore);
        $successor->setFScore($successor->getGScore() + $this->nodeInteractor->getHeuristic($successor));
      }
    }
    
    return null;
  }

  private function extractLeast() {
    if ($this->open->isEmpty()) {
      return null;
    } 

    $leastIdx = 0;
    $least = $this->open->get($leastIdx);
    for ($i = 1; $i < $this->open->length(); $i++) { 
      $n = $this->open->get($i);
      if ($n->getFScore() < $least->getFScore()) {
        $least = $n;
        $leastIdx = $i;
      }
    }
    $this->open->removeAt($leastIdx);
    
    return $least;
  }

  private function reconstructPath($goal) {
    $stack = new Stack();
    $n = $goal;
    while ($n != null) {
      $stack->push($n);
      $n = $n->getPreviousNode();
    }
    return $stack;
  }
}
