<?php

class Movement {
  const UP    = 0;
  const RIGHT = 1;
  const DOWN  = 2;
  const LEFT  = 3;

  public function values() {
    return [
      Movement::UP,
      Movement::RIGHT,
      Movement::DOWN,
      Movement::LEFT
    ];
  }
}
