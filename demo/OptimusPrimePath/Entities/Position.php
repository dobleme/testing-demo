<?php 

require_once __DIR__ . '/Movement.php';

class Position {
  public $row;
  public $column;

  public function __construct($row, $column)
  {
    $this->row = $row;
    $this->column = $column;
  }

  public function getNearPosition($movement) {
    $position = new Position($this->row, $this->column);
    
    switch ($movement) {
      case Movement::UP:
        $position->row++;
        break;
      case Movement::RIGHT:
        $position->column++;
        break;
      case Movement::DOWN:
        $position->row--;
        break;
      case Movement::LEFT:
        $position->column--;
        break;
    }
    
    return $position;
  }

  public function getNearPositions() {
    $nearPositions = [];
    foreach (Movement::values() as $movement) {
      $nearPositions[] = $this->getNearPosition($movement);
    }
    return $nearPositions;
  }

  public function equals($position) {
    if (!$position) {
      return false;
    }

    return 
      $this->row == $position->row &&
      $this->column == $position->column;
  }

  public function hashCode() {
    return 31 * $this->row + $this->column;
  }
}
