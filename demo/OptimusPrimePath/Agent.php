<?php 

require_once __DIR__ . '/Utils/Set.php';
require_once __DIR__ . '/Search/Search.php';
require_once __DIR__ . '/Search/CaveNode.php';
require_once __DIR__ . '/Search/CaveNodeInteractor.php';

class Agent {
  const TRAVEL_COST = 1;
  
  private $visitedCells;
  private $initialPosition;
  private $currentPosition;

  function __construct($initialPosition) {
    $this->visitedCells = new Set();
    $this->initialPosition = $initialPosition;
    $this->addVisitedPosition($initialPosition);
  }

  public function addVisitedPosition($position) {
    $this->currentPosition = $position;
    $this->visitedCells->add($position);
  }

  public function returnExitPath() {
    $nodeInteractor = new CaveNodeInteractor(
      $this->initialPosition, 
      $this->visitedCells
    );
    $search = new Search(static::TRAVEL_COST, $nodeInteractor);
    $currentNode = new CaveNode($this->currentPosition);
    
    return $search->aStar($currentNode);
  }
}
