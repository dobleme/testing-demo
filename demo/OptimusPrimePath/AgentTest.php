<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/Agent.php';
require_once __DIR__ . '/Entities/Position.php';
require_once __DIR__ . '/Search/CaveNode.php';
require_once __DIR__ . '/Utils/Stack.php';

class AgentTest extends TestCase
{
  public function setUp() {
    $this->initialPosition = new Position(0, 0);
    $this->agent = new Agent($this->initialPosition);
  }

  /**
   * @test
   */
  public function whenAgentIntialized_shouldReturnStackExitPath() {
    $this->assertInstanceOf(
      Stack::class, 
      $this->agent->returnExitPath()
    );
  }

  /**
   * @test
   */
  public function whenOneCellVisited_shouldBePathOfTheSameVisitedCell() {
    $exitPath = $this->agent->returnExitPath();
    $node = $exitPath->pop();
    $position = $node->getPosition();

    $this->assertInstanceOf(
      CaveNode::class, 
      $node
    );
    $this->assertTrue($position->equals($this->initialPosition));
  }

  /**
   * @test
   */
  public function whenUniquePath_shouldReturnTheSameExitPath() {
    $positions = [
      new Position(0, 1),
      new Position(1, 1),
      new Position(2, 1),
      new Position(2, 2),
    ];
    
    foreach ($positions as $position) {
      $this->agent->addVisitedPosition($position);
    }
    
    $exitPath = $this->agent->returnExitPath();
    $this->assertEquals(
      count($positions) + 1, 
      $exitPath->length()
    );
    $this->assertPath($exitPath, $positions);
  }

  /**
   * @test
   */
  public function whenNotUniqueExitPaths_shouldRetunrTheOptimusExitPath() {
    $positions = [
      new Position(1, 0),
      new Position(1, 1),
      new Position(2, 1),
      new Position(2, 2),
      new Position(2, 3),
      new Position(1, 3),
      new Position(1, 4),
      new Position(2, 4),
    ];
    
    foreach ($positions as $position) {
      $this->agent->addVisitedPosition($position);
    }

    $expectedOptimumPrimePath = [
      new Position(1, 0),
      new Position(1, 1),
      new Position(2, 1),
      new Position(2, 2),
      new Position(2, 3),
      new Position(2, 4),
    ];
    
    $exitPath = $this->agent->returnExitPath();
    $this->assertEquals(
      count($expectedOptimumPrimePath) + 1, 
      $exitPath->length()
    );
    $this->assertPath($exitPath, $expectedOptimumPrimePath);
  }

  /**
   * @test
   */
  public function whenPathIsIncorrect_shoulReturnNoPath() {
    $positions = [
      new Position(1, 0),
      new Position(1, 1),
      new Position(2, 2),
      new Position(2, 3),
      new Position(2, 4),
    ];
    
    foreach ($positions as $position) {
      $this->agent->addVisitedPosition($position);
    }
    
    $exitPath = $this->agent->returnExitPath();
    $this->assertNull($exitPath);
  }
  
  protected function assertPath($path, $positions) {
    $positions = array_reverse($positions);
    $positions[] = $this->initialPosition;

    $expectedPosition = current($positions);
    while (!$path->isEmpty()) {
      $node = $path->pop();
      $pathPosition = $node->getPosition();
      $this->assertTrue($pathPosition->equals($expectedPosition));
      $expectedPosition = next($positions);
    }
  }
}
