<?php

namespace Habitissimo\Application\Helpers\Tests;

use Habitissimo\Application\Helpers\Str;
use Habitissimo\Tests\TestCase;

class StrTest extends TestCase
{
  public function testTruncate()
  {
    static $cases = [
      ['Lorem ipsum dolor sit', 5, '',    'Lorem'],
      ['Lorem ipsum dolor sit', 6, '',    'Lorem'],
      ['Lorem ipsum dolor sit', 0, '',    'Lorem ipsum dolor sit'],
      ['Una página',            6, '',    'Una pá'],
      ['Una página',            5, '...', 'Una p...'],
      ['Una página',            7, '',    'Una pág'],
      ['5€',                    1, '',    '5'],
      ['5€',                    2, '',    '5€']
    ];

    foreach ($cases as $case) {
      $this->assertEquals(
        $case[3],
        Str::truncate($case[0], $case[1], $case[2])
      );
    }
  }

  public function testTruncateWords()
  {
   static $cases = [
      ['Lorem ipsum dolor sit', 5,  '',    'Lorem'],
      ['Lorem ipsum dolor sit', 6,  '',    'Lorem'],
      ['Lorem ipsum dolor sit', 7,  '',    'Lorem'],
      ['Lorem i d s',           7,  '...', 'Lorem i...'],
      ['Lorem ipsum dolor sit', 7,  '...', 'Lorem...'],
      ['Lorem ipsum dolor sit', 12, '...', 'Lorem ipsum...'],
      ['Lorem ipsum dolor sit', 13, '...', 'Lorem ipsum...'],
      ['Aquí y allí',           4,  '...', 'Aquí...'],
      ['Aquí y allí',           10,  '...', 'Aquí y...'],
      ['Aquí y allí',           11,  '...', 'Aquí y allí'],
      ['Aquí y allí',           99,  '...', 'Aquí y allí'],
    ];

    foreach ($cases as $case) {
      $this->assertEquals(
        $case[3],
        Str::truncateWords($case[0], $case[1], $case[2])
      );
    }
  }

  public function testTitleCase()
  {
    static $cases = [
      ['Lorem ipsum dolor sit', 'Lorem Ipsum Dolor Sit'],
      ['ángela', 'Ángela'],
      ['ÍGOR HERNÁNDEZ', 'Ígor Hernández']
    ];

    foreach ($cases as $case) {
      $this->assertEquals(
        $case[1],
        Str::titleCase($case[0])
      );
    }
  }

  public function testLen()
  {
    static $cases = [
      ['Lorem ipsum dolor sit', 21],
      ['ángela', 6],
      ['ÍGOR HERNÁNDEZ', 14],
      ['solicitação asa', 15],
    ];

    foreach ($cases as $case) {
      $this->assertEquals(
        $case[1],
        Str::len($case[0])
      );
    }
  }

  public function testSub()
  {
    static $cases = [
      ['Lorem ipsum dolor sit', 0, 7, 'Lorem i'],
      ['ángela', 0, 3, 'áng'],
      ['ángela', 0, 1, 'á'],
      ['ÍGOR HERNÁNDEZ', 3, 8, 'R HERNÁN'],
      ['solicitação asa', 0, 13, 'solicitação a'],
    ];

    foreach ($cases as $case) {
      $this->assertEquals(
        $case[3],
        Str::sub($case[0], $case[1], $case[2])
      );
    }
  }

  public function testPos()
  {
    static $cases = [
      ['Lorem ipsum dolor sit', 'i', 6],
      ['ángela', 'á', 0],
      ['ángela', 'e', 3],
      ['ÍGOR HERNÁNDEZ', ' ', 4],
      ['solicitação asa', 'ã', 9],
    ];

    foreach ($cases as $case) {
      $this->assertEquals(
        $case[2],
        Str::pos($case[0], $case[1])
      );
    }
  }

  public function testRpos()
  {
    static $cases = [
      ['Lorem ipsum dolor sit', 'i', 19],
      ['ángela ále', 'á', 7],
      ['ángela', 'e', 3],
      ['ÍGOR HERNÁNDEZ', ' ', 4],
      ['solicitação asa', 'ã', 9],
    ];

    foreach ($cases as $case) {
      $this->assertEquals(
        $case[2],
        Str::rpos($case[0], $case[1])
      );
    }
  }

  public function testImplodeFiltered()
  {
    static $cases = [
      [['','Lorem','ipsum','dolor',null,'sit','i',],'Lorem,ipsum,dolor,sit,i', null],
      [[null,'ángela','ále'],'ángela,ále', null],
      [['ángela', null], 'ángela', null],
      [['ÍGOR',null,'','HERNÁNDEZ'], 'ÍGOR,HERNÁNDEZ', null],
      [['solicitação','asa','',null], 'solicitação,asa', null],
      [['solicitação','asa','',null], 'solicitação,asa', null],
      [[0,1,2,null,'',3,4,5,5], '1,2,3,4,5,5', null],
      [[0,1,2,null,'',3,4,5,5], '0,1,2,,,3,4,5,5', [StrTest::class, 'testImplodeFilteredCallback']],
    ];

    foreach ($cases as $case) {
      $this->assertEquals(
        $case[1],
        Str::implodeFiltered($case[0],',',$case[2])
      );
    }
  }

  public static function testImplodeFilteredCallback($value = null){
    return $value !== false;
  }
}
