<?php

namespace Habitissimo\Application\Helpers\Tests;

use Habitissimo\Application\Helpers\Url;
use Habitissimo\Tests\TestCase;

class UrlTest extends TestCase
{
  public function testMergeParamsToAddQuerystring()
  {
    $this->assertEquals(
      'https://www.google.es?q=test',
      Url::mergeParams('https://www.google.es', ['q' => 'test']));
  }

  public function testMergeParamsToUpdateValueInQuerystring()
  {
    $this->assertEquals(
      'https://www.google.es?q=test',
      Url::mergeParams('https://www.google.es?q=foo', ['q' => 'test']));
  }

  public function testMergeParamsHandlesAcnhor()
  {
    $this->assertEquals(
      'https://www.google.es?q=test#anchor',
      Url::mergeParams('https://www.google.es?q=foo#anchor', ['q' => 'test']));
  }

  public function testMergeParamsRemovesNullParams()
  {
    $this->assertEquals(
      'https://www.google.es?q=foo',
      Url::mergeParams('https://www.google.es?q=foo&s=bar', ['s' => null]));
  }

  public function testMergeParamsRemovesQueryStringIfNoArgs()
  {
    $this->assertEquals(
      'https://www.google.es',
      Url::mergeParams('https://www.google.es?q=foo', ['q' => null]));
  }
}
